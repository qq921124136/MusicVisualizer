package top.xiewenwen.musicvisualizer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements VisualizerInterface {

    public static final int REQUEST_CODE = 100;
    private Visualizer visualizer;
    private ViewPager2 viewPager2;
    private AudioVisualConverter audioVisualConverter;
    private AudioView audioView;
    private AudioView audioView2;
    private TextView tvVoiceSize;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            // 权限未被授予，进行请求
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_CODE);
        }

        viewPager2 = findViewById(R.id.viewPager2);
        ViewPagerAdapter adapter = new ViewPagerAdapter(MainActivity.this,this);
        viewPager2.setAdapter(adapter);
        audioVisualConverter = new AudioVisualConverter();
        audioView = findViewById(R.id.visualizerView);
        audioView.setStyle(AudioView.ShowStyle.STYLE_HOLLOW_LUMP, AudioView.ShowStyle.STYLE_NOTHING);
        audioView2 = findViewById(R.id.FftVisualizerView);
        audioView2.setStyle(AudioView.ShowStyle.STYLE_HOLLOW_LUMP, AudioView.ShowStyle.STYLE_WAVE);
        tvVoiceSize = findViewById(R.id.tvVoiceSize);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 用户授予了权限，可以继续进行操作
            } else {
                // 用户拒绝了权限请求，进行相应的处理，例如提示用户
                // 如果权限未全部授予，则可以根据需求进行处理，比如显示一个提示或者禁用相关功能
                Toast.makeText(this, "未授予必要的权限，无法执行扫描操作", Toast.LENGTH_SHORT).show();

                // 如果需要向用户解释为何需要权限，则弹出解释对话框
                new AlertDialog.Builder(this)
                        .setTitle("需要权限")
                        .setMessage("为了正确运行，应用需要获得录音机权限。")
                        .setPositiveButton("授予权限", (dialog, which) -> {
                            // 直接在这里跳转到设置页面
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        })
                        .setNegativeButton("拒绝", (dialog, which) -> {
                            // 用户拒绝，可以在这里进行处理
                            Toast.makeText(this, "未授予必要的权限，无法执行操作", Toast.LENGTH_SHORT).show();
                        }).show();
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放所有 Visualizer 实例
        visualizer.release();
    }

    @Override
    public void VisualizerGetSessionID(int getAudioSessionId) {
        if(visualizer != null){
            visualizer.release();
        }

        int captureSize = Visualizer.getCaptureSizeRange()[1];
        int captureRate = Visualizer.getMaxCaptureRate() * 3 / 4;
        Log.d("MainActivity", "精度: " + captureSize);
        Log.d("MainActivity", "刷新频率: " + captureRate);
        visualizer = new Visualizer(getAudioSessionId);
        visualizer.setEnabled(false);
        visualizer.setCaptureSize(captureSize);
        visualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            @Override
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] WaveFormData, int samplingRate) {
                audioView.post(new Runnable() {
                    @Override
                    public void run() {
                        audioView.setWaveData(WaveFormData);
                    }
                });
            }

            @Override
            public void onFftDataCapture(Visualizer visualizer, byte[] fftBytes, int samplingRate) {
                audioView2.setWaveData(fftBytes);
                tvVoiceSize.setText(String.format(Locale.getDefault(), "当前分贝: %s db", audioVisualConverter.getVoiceSize(fftBytes)));
            }
        }, captureRate, true, true);
        //visualizer.setScalingMode(Visualizer.SCALING_MODE_NORMALIZED) 是 Android 中 Visualizer 类的一个方法，用于设置可视化数据的缩放模式。
        //SCALING_MODE_NORMALIZED 表示可视化的数据会被标准化处理，即所有数据点会按照相对值来缩放。具体来说，这个模式会根据数据的最大值和最小值进行缩放，使得数据的范围标准化到一个统一的范围（通常是 [0, 1] 或 [-1, 1]），这有助于在视觉上更容易比较不同频率之间的相对强度。
        //使用这个模式的好处是，无论输入音频数据的绝对幅度如何，显示出来的可视化效果都会保持一致的比例，使得视觉效果更具一致性。
        visualizer.setScalingMode(Visualizer.SCALING_MODE_NORMALIZED);
        visualizer.setEnabled(true);
    }

    @Override
    public void VisualizerStop(int getAudioSessionId) {
        if (visualizer != null) {
            // 释放 Visualizer 并从集合中移除
            visualizer.release();
        }
    }

}
