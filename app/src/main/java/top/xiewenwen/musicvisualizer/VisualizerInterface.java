package top.xiewenwen.musicvisualizer;


public interface VisualizerInterface {

    void VisualizerGetSessionID(int getAudioSessionId);
    void VisualizerStop(int getAudioSessionId);
}
