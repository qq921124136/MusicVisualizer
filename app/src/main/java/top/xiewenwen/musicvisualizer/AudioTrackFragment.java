package top.xiewenwen.musicvisualizer;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class AudioTrackFragment extends Fragment {

    private Button but_play;
    private Button but_play2;
    private Button but_play3;
    private Button but_play4;

    private VisualizerInterface visualizerInterface;

    public AudioTrackFragment(VisualizerInterface visualizerInterface){
        this.visualizerInterface = visualizerInterface;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_audio_track, container, false);
        but_play = view.findViewById(R.id.but_play);
        but_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playA4(WaveMode.Sine);
            }
        });
        but_play2 = view.findViewById(R.id.but_play2);
        but_play2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playA4(WaveMode.Square);
            }
        });
        but_play3 = view.findViewById(R.id.but_play3);
        but_play3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playA4(WaveMode.Sawtooth);
            }
        });
        but_play4 = view.findViewById(R.id.but_play4);
        but_play4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playA4(WaveMode.Triangle);
            }
        });
        return view;
    }



    public void playA4(WaveMode wavemode) {
        int SAMPLE_RATE = 44100; // 音频采样率
        int DURATION_MS = 500; // 持续时间（毫秒）
        double A4_FREQUENCY = 440.0; // A4 音符频率
        int numSamples = SAMPLE_RATE * DURATION_MS / 1000;
        double[] samples = new double[numSamples];
        byte[] generatedSnd = new byte[2 * numSamples];

        switch (wavemode) {
            case Sine:{
                for (int i = 0; i < numSamples; i++) {
                    samples[i] = Math.sin(2 * Math.PI * i / (SAMPLE_RATE / A4_FREQUENCY));
                }
                break;
            }
            case Square:{
                for (int i = 0; i < numSamples; i++) {
                    samples[i] = Math.signum(Math.sin(2 * Math.PI * i / (SAMPLE_RATE / A4_FREQUENCY)));
                }
                break;
            }
            case Sawtooth:{
                for (int i = 0; i < numSamples; i++) {
                    samples[i] = 2.0 * (i / (double)(SAMPLE_RATE / A4_FREQUENCY) - Math.floor(i / (double)(SAMPLE_RATE / A4_FREQUENCY) + 0.5));
                }
                break;
            }
            case Triangle:{
                for (int i = 0; i < numSamples; i++) {
                    samples[i] = 2.0 * Math.abs(2.0 * (i / (double)(SAMPLE_RATE / A4_FREQUENCY) - Math.floor(i / (double)(SAMPLE_RATE / A4_FREQUENCY) + 0.5))) - 1.0;
                }
                break;
            }
        }

        // 将数据转换为 PCM 格式
        int idx = 0;
        for (double sample : samples) {
            short value = (short) (sample * 32767);
            generatedSnd[idx++] = (byte) (value & 0x00ff);
            generatedSnd[idx++] = (byte) ((value & 0xff00) >>> 8);
        }

        // 播放音频数据
        AudioTrack audioTrack = new AudioTrack(
                AudioManager.STREAM_MUSIC,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                generatedSnd.length,
                AudioTrack.MODE_STATIC
        );

        visualizerInterface.VisualizerGetSessionID(audioTrack.getAudioSessionId());
        audioTrack.write(generatedSnd, 0, generatedSnd.length);
        audioTrack.play();
    }

    public enum WaveMode {
        Sine,
        Square,
        Sawtooth,
        Triangle,
    }

}