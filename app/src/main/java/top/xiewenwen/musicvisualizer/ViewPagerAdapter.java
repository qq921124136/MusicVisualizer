package top.xiewenwen.musicvisualizer;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;


public class ViewPagerAdapter extends FragmentStateAdapter {

    private VisualizerInterface visualizerInterface;

    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity, VisualizerInterface visualizerInterface) {
        super(fragmentActivity);
        this.visualizerInterface = visualizerInterface;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new AudioTrackFragment(visualizerInterface);
            case 1:
                return new MusicFragment(visualizerInterface);
            case 2:
                return new RecordFragment(visualizerInterface);
            default:
                return new AudioTrackFragment(visualizerInterface);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

}